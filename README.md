# Workshop jazyka Python a strojového učení

Veřejný repositář kurzu Základy Python a strojového učení. Repozitář slouží k poskytnutí podpůrného materiálu.</br>

## Základy jazyka Python: </br>
    1.12. od 15-17 hod – Seznámení s Google Colab, základní syntaxe, práce se smyčkami, podmínkami, zobrazení grafů, obrázků 
    8.12. od 15-17 hod – Práce se slovníky a sety, metody a třídy, numpy, torch, obecná příprava dat (csv, json) 
## Základy strojového učení v jazyce Python: </br>
    15.12. od 15-17 hod – Implementace klasifikace pomocí CNN na obrazových datech 
    22.12. od 10-12 hod – Implementace RNN/CNN pro práci se signálem 

Autoři: Tomáš Gotthans, Jakub Gotthans (Vysoké učení technické v Brně) </br>
2021, gotthans@vutbr.cz </br>
